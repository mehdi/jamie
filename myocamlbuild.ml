(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Ocamlbuild_plugin

let names = [ "maddie" ]
let packages = [
  "unix";
  "str";
  "curl";
  "zip";
  "extlib";
  "dose3.versioning";
  "calendar";
  "tyxml";
  "xmlm";
]

let __ = Printf.sprintf

exception Require_findlib
exception Missing_findlib_package of string

let try_exec cmd =
  Sys.command (__ "%s >/dev/null 2>&1" cmd) = 0

let require pkg =
  if not (try_exec (__ "ocamlfind query %s" pkg)) then
    raise (Missing_findlib_package pkg)

let ocamlfind x = S[A"ocamlfind"; A x]
let has_ocamlopt = try_exec "which ocamlopt"
let best = if has_ocamlopt then "native" else "byte"
let compiler = if has_ocamlopt then Options.ocamlopt else Options.ocamlc
let _ = if not (try_exec "ocamlfind printconf") then raise Require_findlib
let _ = List.iter require packages

let natives = List.map (Printf.sprintf "%s.native") names
let bytes = List.map (Printf.sprintf "%s.byte") names

let list = function
  | "all" | "native" -> natives
  | _ -> bytes

let _ =
  let args = Array.to_list Sys.argv in
  if List.mem "clean" args then begin
    Options.must_clean := true;
    ignore(Sys.command "rm -Rf *.dot *.png");
  end

let _ =
  dispatch begin function

    | Before_options ->
        Ocamlbuild_pack.Log.classic_display := true;
        Options.ocamlc := ocamlfind "ocamlc";
        Options.ocamlopt := ocamlfind "ocamlopt";
        Options.ocamldep := ocamlfind "ocamldep";
        Options.ocamldoc := ocamlfind "ocamldoc"

    | After_rules ->
        flag ["ocaml"; "link"; "program"] & A"-linkpkg";
        List.iter
          (fun pkg ->
             let flag x = flag (x::["ocaml"]) & S[A"-package"; A pkg] in
             List.iter flag ["ocamldep"; "compile"; "link"; "doc"])
          packages;

        let virtual_rule name deps =
          rule name ~stamp:name ~deps
            (fun _ _ ->
               Seq (List.map (fun name ->
                           let target = Filename.chop_extension name in
                           Cmd (Sh (__ "cp %s %s" name target))) (list name)
                   )
            )
        in

        virtual_rule "all" & natives @ bytes;
        virtual_rule "native" & natives;
        virtual_rule "byte" & bytes

    | _ -> ()
  end
