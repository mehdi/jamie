(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Tyxml.Html
open Printers
open Grab
open Misc

let output = ref ""
let update = ref false
let multi = ref false
let patches = ref true
let conf = ref ""
let freq = ref "day"

let anon s =
  conf := s

let spec = Arg.align [
  "-mf", Arg.Int (fun i -> Diff.max_files := Some i), " Set max files changed per package";
  "-ml", Arg.Int (fun i -> Diff.max_lines := Some i), " Set max lines changed per file";
  "-quiet", Arg.Set quiet        , " Quiet mode";
  "-multi", Arg.Set multi        , " Multiple-files mode";
  "-freq" , Arg.Set_string freq  , " Set update frequency (default: day)";
  "-no-patches", Arg.Clear patches , " Don't show modified files for each package";
  "-o"  , Arg.Set_string output  , " Path of the generated file(s)";
  "-f"  , Arg.Set update         , " Force update of Sources and Packages files";
]

let usage =
  let program = Sys.argv.(0) in
  __ "Usage: %s [options] request.xml" program
let _ = Arg.parse spec anon usage
let _ = Config.read Config.Maddie !conf

let _ = Grab.tiny_init ()

let grab_init grab file =
  if !update || not (Sys.file_exists file) then grab file

let _ = grab_init
  (download ~compression:false "https://patches.ubuntu.com/PATCHES")
  (!cache / "PATCHES")
let _ = Patches.init (!cache/"PATCHES")

let dfile, debian =
  mirror := "http://deb.debian.org/debian";
  areas := [ "main"; "contrib"; "non-free" ];
  !cache/"Sources.Debian" , Hashtbl.create 97
let _ = grab_init (Grab.szget "unstable") dfile
let _ = Units.init debian dfile

let ufile, ubuntu =
  mirror := "http://archive.ubuntu.com/ubuntu/";
  areas := [ "main"; "universe"; "multiverse"; "restricted" ];
  !cache/"Sources.Ubuntu" , Hashtbl.create 97
let _ = grab_init (Grab.szget "devel") ufile
let _ = Units.init ubuntu ufile

let _ = Curl.global_cleanup ()

let warn package =
  _e "W: %s\n%!" package

let info package =
  _d "I: %s\n%!" package

let a_link href contents =
  a ~a:[a_href (uri_of_string href)] [txt contents]

let list = Queue.create ()

let remove_epoch version =
  try
    let pos = String.index version ':' in
    let len = String.length version in
    String.sub version (pos + 1) (len - pos - 1)
  with Not_found -> version

let temp = Filename.temp_file "/tmp" ""
let pemt = Filename.temp_file "/tmp" ""
let number = ref 0
let sep = txt " | "

let rec itemize label bd ed list =
  let rec aux ed = function
    | [] -> [ ed ]
    | [h] -> h :: [ ed ]
    | h::l -> h :: sep :: (aux ed l)
  in
  b [ txt label ] :: bd :: (aux ed list)

let packages =
  let list = Query.search (Config.get "query") in
  let packages = Hashtbl.fold
    (fun package patch accu ->
       if List.mem package list
       then (package, patch) :: accu
       else accu
    )
    Patches.db
    [] in
  let compare (a,_) (b,_) = String.compare a b in
  List.sort compare packages

let l_link fmt x name =
  a_link (__ fmt x) name
let l_pts pkg =
  l_link "https://packages.qa.debian.org/%s" pkg "PTS"
let l_dbugs pkg =
  l_link "https://bugs.debian.org/src:%s" pkg "Bugs"
let l_ubugs pkg =
  l_link "https://bugs.launchpad.net/ubuntu/+source/%s" pkg "Bugs"
let l_lp pkg =
  l_link "https://launchpad.net/ubuntu/+source/%s" pkg "LP"
let l_dch dir =
  l_link "https://packages.debian.org/changelogs/%s/current/changelog" dir "Changelog"
let l_uch pkg =
  l_link "https://launchpad.net/ubuntu/+source/%s/+changelog" pkg "Changelog"
let l_txtch dir pkg ver =
  a_link (__ "http://changelogs.ubuntu.com/changelogs/%s/%s_%s/changelog" dir pkg ver) "txt"
let l_patch patch sep =
  let prefix = "https://patches.ubuntu.com/" in
  sep :: [ a_link (__ "%s%s" prefix patch) "Diff from Ubuntu" ]

let hunk_style =
  if !multi
  then a_class ["hunks"]
  else a_class []

let li_hunk_style file =
  let id = __ "#hunk_%s_details" file in
  [ a_id id; a_class [ "hunk_details" ] ]

let li_header_style large file =
  let id = __ "#hunk_%s" file in
  let css_class = if large then [ "hunk_too_large" ] else [ "hunk_header" ] in
  [ a_id id; a_class css_class ]

let hunks package patch dir =
  let dir = if !multi then Some dir else None in
  let hunks = List.fold_left
    (fun acc (file, large, link, hunks) ->
      let id = Mylib.escape_name file in
      let id = __ "%s_%s" package id in
      let hunk_style = li_hunk_style id in
      let nunk_style =
        if String.compare file "..." = 0
        then []
        else li_header_style large id in
      let patch_link =
        if !multi && String.compare file "..." <> 0 then
          [ a ~a:[a_href (uri_of_string link)]
            [img ~src:(uri_of_string "download.png")
               ~alt:"Download patch"
               ~a:[ a_class [ "a_img" ] ] ()
            ];
            txt " "
          ]
        else [ txt "" ]
      in
      let hunks = (br () :: hunks) @ [ br () ] in
      let hunk_div =
        span ~a:nunk_style [ txt file ]
        :: (if large then [] else [ div ~a:hunk_style hunks ]) in
      let change = li (patch_link @ hunk_div) in
      change :: acc)
    []
    (Diff.read package patch pemt dir)
  in
  let hunks =
    match hunks with
    | [] -> [ li [ div [ txt "There are no changes" ] ] ]
    | _ -> hunks
  in
  let ol = [ ol hunks ] in
  div ( br () :: (b [txt "Modifications :"]) :: ol )

let compute_hunks package dir =
  if !patches
  then fun patch -> hunks package (__ "https://patches.ubuntu.com/%s" patch) dir
  else fun _ -> br ()

let changes dir write package patch_url =
  let dinfos = Hashtbl.find debian package in
  let uinfos = Hashtbl.find ubuntu package in
  let ch_url = __ "http://changelogs.ubuntu.com/changelogs/%s/%s_%s/changelog"
    uinfos.Units.directory
    package
    (remove_epoch uinfos.Units.version) in
  let pkg = __ "%s%s/%s" !mirror uinfos.Units.directory package in
  let code, changelog = Changelog.parse pkg temp ch_url in
  begin
    info package;
    incr number;
    Queue.add package list;
    let patch = l_patch patch_url sep in
    let pts =  l_pts package in
    let dbugs = l_dbugs package in
    let lp = l_lp package in
    let dch = l_dch dinfos.Units.directory in
    let uch = l_uch package in
    let ubugs = l_ubugs package in
    let uuch = sup [ a_link ch_url "txt" ] in
    let changes =
      div
        [
          b [ txt "Source: " ];
          span ~a:[a_class ["pkg_name"]] [ txt package ];
          if code
          then div [ br (); span ~a:[a_class [ "changelog"  ]] changelog ]
          else div [ br ();
                     txt "The ";
                     a_link ch_url "Changelog";
                     txt " was not found or an error occured while reading it!" ]
        ] in
    let modifications = compute_hunks package dir patch_url in
    let bd = txt " ( " in
    let ed = txt " ) " in
    let changes =
      div ~a:[ a_id package; a_class ["changes"] ]
        [ p (  itemize "Debian" bd ed [dch; pts; dbugs]
             @ itemize "Ubuntu" bd ed [uch; uuch; lp;  ubugs]
             @ patch
            );
          changes;
          modifications
        ] in
    write package changes
  end

let _ = Sys.remove temp
let _ = Sys.remove pemt

let html_body divs =
  if !number > 0 then
    let list =
      Queue.fold
        (fun accu package ->
           let url = __ "#%s" package in
           (li [a_link url package]) :: accu
        )
        []
        list in
    let list = div ~a:[a_id "frame"]
      [ ol list ] in
    list :: divs
  else [ h3 [span [txt "No changes found!"]]]

let footer () =
  let date = CalendarLib.Printer.CalendarPrinter.to_string
    (CalendarLib.Calendar.now ()) in
  let src_webbrowse_url =
    "https://salsa.debian.org/mehdi/jamie" in
  div ~a:[a_id "footer"] [txt "Last generated: ";
       span ~a:[a_class ["timestamp"]] [txt date];
       txt (__ " (page currently rebuilt every %s)." !freq);
       br ();
       txt "Contact: ";
       a_link "https://wiki.debian.org/MehdiDogguy" "Mehdi Dogguy";
       txt "."; br ();
       txt "The code that generates this page is free software and ";
       a_link src_webbrowse_url "is available";
       txt "."; br ();
       txt "This page has been type-checked by OCaml using ";
       a_link "https://ocsigen.org/tyxml/" "tyxml";
       txt " and should be ";
       a_link "http://validator.w3.org/check?uri=referer" "XHTML Valid";
       txt "."
      ]

let span_query l =
  span ~a:[a_class ["query"]] l

let html
    ?(js = false)
    ?(query = [ pre [ txt "" ] ])
    _body =
  let css_style = [
    txt "
    ";
    txt " img#ubuntu { border: 0; }
    ";
    txt " img#debian { float: right; border: 0; }
    ";
    txt " div#frame { position: absolute; top: 12em;
    right: .1em; padding: .5em; list-style-type: none;
    border: thin solid black; background: #ddd;
    overflow: auto; max-width: 30%; }
    ";
    txt " div.changes { border: 1px solid #999999;
    background: #ffffd1; padding: 0.5em; margin: 1em; }
    ";
    txt " span.pkg_name { color: red; font-weight: bold; }
    ";
    txt " span.query { font-style:italic; font-size: 150%; }
    ";
    txt " div.pkg { font-size:130% }
    ";
    txt " div.nochanges { border: 1px solid #ff9999;
    background: #ffffd1; padding: 0.5em; margin: 1em;
    text-align: center; font-size: 150%; }
    ";
    txt " div#footer { font-size: 70%; }
    ";
    txt " div#help { font-size: 70%; }
    ";
    txt " .a_img { border: 0px; }
    ";
    txt " .hunk_too_large { color: red; }
    ";
    txt " .changelog { font-family: monospace; white-space: pre }
    ";
    txt " .patch { font-family: monospace; white-space: pre }
    ";
    txt " .patch_addition { color: #00A000 }
    ";
    txt " .patch_deletion { color: #A00000 }
    ";
    txt " .patch_offsets { color: #800080; font-weight: bold }
    "
  ] in
  let header, help =
    if not js then [], []
    else [
      script
        ~a:[a_src (uri_of_string "https://code.jquery.com/jquery-latest.js")]
        (txt "");
      script
        ~a:[a_src (uri_of_string ("maddie.js"))]
        (txt "");
    ],
    [div ~a:[a_id "help"] [
     ]]
  in
  html ~a:[a_xmlns `W3_org_1999_xhtml]
    (head (title (txt (__ "%s: Available patches from Ubuntu" (Config.get "name"))))
       ([
          style css_style;
          meta ~a:[a_charset "utf-8"; a_http_equiv "Content-Type"] ();
        ] @ header))
    (body
       ([ div [
            img
              ~src:(uri_of_string "https://www.debian.org/logos/openlogo-100.png")
              ~alt:"Debian"
              ~a:[a_id "debian"] ()];
          h1 [txt "Available patches from Ubuntu"];
          div [ txt "To see Ubuntu differences wrt. to Debian, write down a grep-dctrl query identifying the packages you're interested in:" ];
          div [
            let div =
              div (
                 [ span_query [ a_link "http://manpages.debian.org/cgi-bin/man.cgi?query=grep-dctrl" "grep-dctrl"; txt " -n "] ]
                 @ query
                 @ [ span_query [ txt " -sPackage Sources.Debian" ] ]
                 @ help
               )
            in
            form ~a:[a_id "searchf"; a_method `Get; a_action (uri_of_string "")]
              [ div ]
          ];
          h3 [txt (Config.get "description")]
        ]
        @ _body @ [footer ()]
       )
    )

let remove_extension filename =
  let file = Filename.basename filename in
  let name =
    try
      (Filename.chop_extension file)
    with _ -> file
  in
  Filename.concat (Filename.dirname filename) name

let commit_multi_package ?(ext="html") ?(index=None) print dir package div =
  let filename = __ "%s.%s" package ext in
  let filename = dir/filename in
  let flags = [ Open_trunc; Open_wronly; Open_creat; Open_text ] in
  let out = open_out_gen flags 0o644 filename in
  let _ = Option.map_default
    (fun o ->  output_string o (__ "%s\n" package)) () index in
  let _ = print out div in
  close_out out

let commit_single_package package div =
  div

let unsafe_data s = Tyxml.Html.Unsafe.data s

let dispatch_multi dir =
  let new_dir = dir/"packages" in
  let _ = Mylib.mkdir new_dir in
  let index = new_dir/"list.txt" in
  let index = open_out_gen
    [ Open_append; Open_wronly; Open_creat; Open_text ]
    0o644
    index in
  let write =
    commit_multi_package ~index:(Some index) Mylib.elt_print new_dir in
  let _ = List.iter
    (fun (package, patch) ->
     try
       changes new_dir write package patch
     with
       | Not_found -> warn package
    )
    packages in
  let _ = close_out index in
  let _body = [
    div ~a:[a_id "results"] [unsafe_data "<?php $list = proceed ($request); ?>"];
    div ~a:[a_id "frame"] [
      input ~a:[a_id "clear"; a_input_type `Button; a_value "Clear results"] ();
      unsafe_data "<ol id=\"pkglist\"><?php echo $list; ?></ol>"
    ]
  ] in
  let query = [
    unsafe_data "<?php
  $request = '';
  if (isset($_GET['x'])) $request .= '-X ';
  if (! empty($_GET['f']) && !empty($_GET['q'])) {
    $request .= '-F'.ucwords($_GET['f']).' '.$_GET['q'];
  }
  if (! empty($_GET['query'])) $request .= $_GET['query'];
  require_once ('maddie.php'); ?>
";
    unsafe_data "<input name=\"query\" id=\"searchq\" type=\"text\" size=\"50\" value=\"<?php echo $request; ?>\" />";
    input ~a:[a_input_type `Submit; a_value "Search" ] ()
  ] in
  let ext = "php" in
  let html = html
    ~js:true
    ~query
    _body in
  commit_multi_package ~ext ~index:None Mylib.html_print dir "index" html

let dispatch_single file =
  let fid _ x = x in
  let divs =
    List.fold_left
      (fun accu (package, patch) ->
         try
           (changes "" fid package patch) :: accu
         with
           | Not_found -> warn package; accu
      )
      []
      packages in
  let _body = html_body divs in
  let query = [ span_query [ txt (Config.get "query") ] ] in
  let html = html
    ~query
    _body in
  let fd = open_out file in
  Mylib.html_print fd html;
  close_out fd

let choose_output default =
  if !output = ""
  then default
  else !output

let _ =
  let file = remove_extension !conf in
  if !multi then
    let dir = choose_output file in
    dispatch_multi dir
  else begin
    let filename = __ "%s.html" file in
    let file = choose_output filename in
    dispatch_single file
  end
