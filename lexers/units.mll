(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Hashtbl
  open Misc

  type unit = { version : string ; directory : string }
  let create_unit version directory =
    { version = version; directory = directory }
  let add_package db pkg info =
    try
      let unit = find db pkg in
      if Dose_versioning.Debian.compare info.version unit.version > 0
      then replace db pkg info
    with Not_found -> add db pkg info

  let _p x = Printf.printf "%s\n%!" x
  let _s x = Printf.sprintf x

}

let package_name  = ['A'-'Z' 'a'-'z' '-' '+' '0'-'9' '.']+

rule entry db = parse
  | "Package: " (package_name as pkg) '\n'
      { newline lexbuf; version db pkg lexbuf }
  | [^ '\n'] | "" { skip (entry db) lexbuf }
  | eof { () }

and version db pkg = parse
  | "Version: " ([^ '\n']+ as version) '\n'
      { newline lexbuf; directory db pkg version lexbuf }
  | _ { skip (version db pkg) lexbuf }

and directory db pkg ver = parse
  | "Directory: " ([^ '\n']+ as directory) '\n'
      { let unit = create_unit ver directory in
        add_package db pkg unit;
        newline lexbuf;
        entry db lexbuf
      }
  | _ { skip (directory db pkg ver) lexbuf }

and skip rulez = parse
  | [^ '\n' ]* "\n" { newline lexbuf; rulez lexbuf }
  | "" { () }

{

  let init db file =
    parse_init (entry db) file

}
