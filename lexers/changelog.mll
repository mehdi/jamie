(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Printers
  open Lexing
  open Tyxml.Html

  type changelog_t = ([ `A of Html_types.phrasing_without_interactive | `PCDATA ] Tyxml.Html.elt) list

  let buf = Buffer.create 128
  let que : changelog_t ref = ref []
  let first = ref true
  let suites = [ "unstable"; "testing"; "experimental" ]

  let push elt = que := elt :: !que
  let clear () = que := []

  let dsc pkg ver =
    let link = __ "%s_%s.dsc" pkg ver in
    a ~a:[a_href (uri_of_string link)] [ txt ver ]

  let qa pkg =
    let link = __ "http://packages.qa.debian.org/%s" pkg in
    a ~a:[a_href (uri_of_string link)] [ txt pkg ]

  let pts mail =
    let link = __ "http://qa.debian.org/developer.php?login=%s" mail in
    a ~a:[a_href (uri_of_string link)] [ txt mail ]

  let return () =
    push (txt (Buffer.contents buf));
    let result = List.rev !que in
    Buffer.reset buf;
    clear ();
    true, result

  let is_binnmu =
    let regexp = Str.regexp ".*build[0-9]+$" in
    fun version ->
      try
        Str.search_forward regexp version 0 >= 0
      with _ -> false

  exception Not_available

}

let pkg_name  = ['A'-'Z' 'a'-'z' '-' '+' '0'-'9' '.']+
let string = [^ '\n']+

rule entry pkg sep = parse
  | "<title>404 Not Found</title>\n"
      {
        Buffer.reset buf;
        raise Not_available
      }
  | (pkg_name as src) " (" (string as version)  ") " ((string as suite) "; urgency=" string "\n" as tail)
      {
        if !first && is_binnmu version then
          skip pkg sep lexbuf
        else
        if List.mem suite suites then
          return ()
        else begin
          let dsc = dsc pkg version in
          let dsc = if !first then dsc else txt version in
          first := false;
          Buffer.add_string buf sep;
          let content = txt (Buffer.contents buf) in
          Buffer.reset buf;
          let content = [
            content;
            qa src;
            txt " (";
            dsc;
            txt ") ";
            txt tail
          ] in
          List.iter push content;
          entry pkg "" lexbuf
        end
      }
  | " -- " ([^ '<']+ as name) " <" ([^ '>']+ as mail) "> " ([^ '\n']* '\n' as tail)
      {
        Buffer.add_string buf sep;
        let content = txt (Buffer.contents buf) in
        Buffer.reset buf;
        let content = [
          content;
          txt (__ " -- %s <" name);
          pts mail;
          txt (__ "> %s" tail);
        ] in
        List.iter push content;
        entry pkg sep lexbuf
      }
  | "\n"
      {
        entry pkg "\n" lexbuf
      }
  | [^ '\n']+ '\n' as line
      {
        Buffer.add_string buf sep;
        Buffer.add_string buf line;
        entry pkg "" lexbuf
      }
  | eof { return () }

and skip pkg sep = parse
  | " " [^ '\n']* "\n"
      {
        skip pkg sep lexbuf
      }
  | " -- " [^ '\n']* "\n"
      {
        entry pkg sep lexbuf
      }
  | eof | "" { return () }

{

  let parse pkg temp url =
    let _ = Grab.download ~mode:Open_trunc ~compression:false url temp in
    Misc.with_file temp
      begin fun chan ->
        let lexbuf = from_channel chan in
        try
          Buffer.reset buf;
          first := true;
          entry pkg "" lexbuf
        with
          | Not_available -> false, []
          | Failure _ ->
              let b = (lexeme_start_p lexbuf)
              and e = (lexeme_end_p lexbuf)
              in let loc = Misc.loc_to_string b e in
              Printf.eprintf "%s\nE: Error while reading \"%s\" in %s\n%!"
                loc
                (lexeme lexbuf)
                url;
              false, []
      end

}
