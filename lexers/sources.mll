(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Hashtbl
  open Misc
  open Lexing

  let sources = create 97 (* Mapping bin -> src *)
  let depends = create 97 (* Mapping src -> build-deps *)

  let add_dep src dep =
    let deps =
      try find depends src
      with Not_found -> [] in
    if not (List.mem dep deps) then (
      replace depends src (dep :: deps)
    )

  let newline = Misc.newline
}

let package_name  = ['A'-'Z' 'a'-'z' '-' '+' '0'-'9' '.']+
let comment_begin = [ '(' '[' ]
let comment_end   = [ ']' ')' ]

rule entry = parse
  | "Package: " (package_name as src) '\n' {
      if not (mem depends src) then add depends src [];
      binary src lexbuf
    }
  | [^ '\n']* '\n' { newline lexbuf; entry lexbuf }
  | eof | "" { () }

and binary src = parse
  | "Binary: " { binary_contents src lexbuf }

and binary_contents src = parse
  | package_name as bin {
      replace sources bin src;
      binary_contents src lexbuf
    }
  | [' ' ','] { binary_contents src lexbuf }
  | '\n' { newline lexbuf; dependencies src lexbuf }

and dependencies src = parse
  | ([^':' '\n']+ as field) ": " {
      if List.mem field [ "Build-Depends"; "Build-Depends-Indep" ]
      then dependencies_list src lexbuf
      else skip (dependencies src) lexbuf
    }
  | '\n' { newline lexbuf; entry lexbuf }
  | "" { entry lexbuf }

and dependencies_list src = parse
  | package_name as dep {
      add_dep src dep;
      dependencies_list src lexbuf
    }
  | comment_begin { comment src lexbuf }
  | [' ' ',' '|'] { dependencies_list src lexbuf }
  | '\n' { newline lexbuf; dependencies src lexbuf }

and comment src = parse
  | comment_end { dependencies_list src lexbuf }
  | [^ '\n'] { comment src lexbuf }

and skip rulez = parse
  | [^ '\n'] { skip rulez lexbuf }
  | '\n' { newline lexbuf; rulez lexbuf }

{

  let init () =
    parse_init entry !Grab.sources

}
