(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Hashtbl
  open Misc

  let virtuals = create 97 (* Mapping virtual -> [bin] *)

  let add_virtual virts package =
    List.iter
      (fun virt ->
         try
           replace virtuals virt
             (package :: (find virtuals virt))
         with _ ->
           add virtuals virt [package]
      )
      virts

}

let package_name  = ['A'-'Z' 'a'-'z' '-' '+' '0'-'9' '.']+
let comment_begin = [ '(' '[' ]
let comment_end   = [ ']' ')' ]
let dep_schema    = comment_begin [^'\n' ')' ']']+ comment_end

rule entry = parse
  | "Package: " (package_name as pkg) '\n'
      {
        provides pkg lexbuf
      }
  | [^ '\n']* "\n" { entry lexbuf }
  | eof { () }

and provides package = parse
  | "Provides:" { provides_contents package [] lexbuf }
  | _ { skip package lexbuf }

and provides_contents package accu = parse
  | package_name as x { provides_contents package (x::accu) lexbuf }
  | [' ' ',']+ { provides_contents package accu lexbuf }
  | "\n" {
      add_virtual accu package;
      entry lexbuf
    }

and skip package = parse
  | [^ '\n' ]* "\n" { provides package lexbuf }

{

  let init () =
    parse_init entry !Grab.packages

}
