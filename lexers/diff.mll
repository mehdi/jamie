(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Misc
  open Printers
  open Tyxml.Html

  let buf = Buffer.create 4096
  let patch = Buffer.create 4096
  let que = (ref [] : ([ `Br | `Span ] Tyxml.Html.elt) list ref)
  let set = Hashtbl.create 17
  let lines = ref 0
  let files = ref 0

  let max_lines = ref None
  let max_files = ref None

  let ignored max t () =
    match max with
    | Some n when n < !lines -> true
    | _ -> false

  let ignored_lines = ignored !max_lines !lines
  let ignored_files = ignored !max_files !files

  let push elt = que := elt :: !que
  let clear () = que := []

  let addition = [a_class ["patch"; "patch_addition"]]
  let deletion = [a_class ["patch"; "patch_deletion"]]
  let offsets = [a_class ["patch"; "patch_offsets"]]
  let context = [a_class ["patch"]]

  let banned_files = [ "debian/changelog" ]
  let is_valid file = file <> ""

  let save_buffer () =
    let content = Buffer.contents buf in
    let _ = Buffer.reset buf in
    if String.length content > 0 then
      let div = span ~a:context [ txt content ] in
      push div

  let hunks file =
    save_buffer ();
    List.rev !que

  let dump_patch pkg file changes dir =
    let filename = Mylib.escape_name file in
    let filename = __ "%s.patch" filename in
    let destination = dir/filename in
    let _ = Std.output_file destination changes in
    let filename = pkg/filename in
    "packages"/filename

  let save dir pkg file =
    save_buffer ();
    if is_valid file then begin
      if not (List.mem file banned_files) && (ignored_files ()) then begin
        let _ = incr files in
        let diff = Buffer.contents patch in
        let dir = Option.map_default (fun x -> x) "" dir in
        let link = dump_patch pkg file diff dir in
        if ignored_lines () then
          Hashtbl.add set file (link, true, [ (* This part is be ignored *) ])
        else
          Hashtbl.add set file (link, false, hunks file)
      end;
    end;
    lines := 0;
    clear ();
    Buffer.reset buf;
    Buffer.reset patch

  let return () =
    let list = Hashtbl.fold
      (fun file (link, large, changes) list ->
        (file, large, link, changes) :: list )
      set
      []
    in
    Hashtbl.clear set;
    if ignored_files ()
    then ( "...", true, "", [] ) :: list
    else list

}

rule entry pkg dir file = parse
  | "diff " [^ ' ']+ ' ' ([^ ' ']+ as debian) ' ' [^ ' ']+ '\n'
      {
        save dir pkg file;
        let pos = (String.index debian '/') + 1 in
        let len = String.length debian in
        let new_file = String.sub debian pos (len - pos) in
        entry pkg dir new_file lexbuf
      }
  | (['@' '+' '-' ' '] [^ '\n']* '\n') as line
      {
        incr lines;
        save_buffer ();
        let a = match line.[0] with
          | '@' -> offsets
          | '+' -> addition
          | '-' -> deletion
          | _ -> context in
        push (span ~a [ txt line ]);
        Buffer.add_string patch line;
        entry pkg dir file lexbuf
      }
  | ([' ']? [^'\n']* '\n') as line
      {
        incr lines;
        Buffer.add_string buf line;
        Buffer.add_string patch line;
        entry pkg dir file lexbuf
      }
  | eof
      {
        save dir pkg file
      }

{

  let read pkg url file dir =
    let _ = files := 0 in
    let _ = lines := 0 in
    let _ = Hashtbl.clear set in
    let _ = Buffer.reset buf in
    let _ = Buffer.reset patch in
    let _ = clear () in
    let _ = Grab.download ~mode:Open_trunc ~compression:false url file in
    let dir = Option.map_default (fun dir -> Some (dir/pkg)) None dir in
    let _ = Option.map_default
      (fun dir -> Sys.command (__ "mkdir -p %s" dir)) 0 dir in
    let _ = Misc.parse_init (entry pkg dir "") file in
    return ()

}
