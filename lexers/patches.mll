(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Misc
  open Tyxml.Html

  let db = Hashtbl.create 1031
  let __ = Printf.sprintf

}

let package_name  = ['A'-'Z' 'a'-'z' '-' '+' '0'-'9' '.']+
let string = [^ '\n']*

rule entry = parse
  | (package_name as pkg) " " (string as patch) "\n"
      { Hashtbl.add db pkg patch;
        newline lexbuf;
        entry lexbuf
      }
  | eof | "" { () }

{

  let init file =
    parse_init entry file

}
