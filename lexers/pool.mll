(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

{

  open Printers
  open Misc

  let transitions = Hashtbl.create 97

  let new_transition name query =
    Hashtbl.replace transitions name query

}

let blank = [ '\t' ' ']
let sep = blank*
let name  = [^ '\n' '=' '\t']* [^ '\n' '=' '\t' ' ' ]
let comment_begin = [ '(' '[' ]
let comment_end   = [ ']' ')' ]
let dep_schema    = comment_begin [^'\n' ')' ']']+ comment_end

rule entry current = parse
  | '/' '/' [^ '\n']* '\n'
  | '#' [^ '\n']* '\n'
      {
        newline lexbuf;
        entry current lexbuf
      }
  | (name as name) sep '=' sep ([^ '\n']* as query) '\n'
      {
        newline lexbuf;
        new_transition name query;
        entry name lexbuf
      }
  | blank+ ([^ '\n' '=' ]+ as query) '\n'
      {
        newline lexbuf;
        if current <> "" then
          let request = try Hashtbl.find transitions current with _ -> "" in
          new_transition current (__ "%s %s" request query);
        entry current lexbuf
      }
  | sep '\n'
      {
        newline lexbuf;
        entry current lexbuf
      }
  | eof { () }
  | _ { raise (Failure "") }

{

  let read file =
    parse_init (entry "") file

}
