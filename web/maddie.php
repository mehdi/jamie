<?php

/****************************************************************************/
/*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  */
/*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        */
/*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           */
/*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      */
/*                                                software: you can         */
/*   redistribute it and/or modify it under the terms of the GNU General    */
/*   Public License as published by the Free Software Foundation, either    */
/*   version 3 of the License, or (at your option) any later version.       */
/****************************************************************************/

function not_found ($request) {
    $msg = "";
    if (!empty($request)) $msg = "<br /><sub>$request</sub>";
    echo '<div class="nochanges">No changes found!'.$msg.'</div>';
}

function error_pkg($package) {
    echo '<div id="'.$package.'" class="changes">'
        .' Source: <span class="pkg_name">'.$package.'</span>'
        .' <pre>An error occured while loading changes.</pre>'
        .'</div>';
}

function proceed($request) {
    $path = "..";
    $found = array();
    $list = "";
    $i = 0;
    if (! preg_match('@[\\$`;&/\\\\]@i', $request)                             // not malicious
     && ! preg_match('/\b(--copying|-C|--help|-h|--version|-V)\b/i', $request) // not stupid
     && ! preg_match('@^[ \t]*$@', $request)                                   // not empty
     ) {
        $handle = popen("grep-dctrl -n $request -sPackage $path/Sources.Debian", "r");
        if ($handle) {
            while (!feof($handle)) {
                $package = trim(fgets($handle));
                $file = "packages/".$package.".html";
                if (file_exists($file) && ! in_array($package, $found)) {
                    $html = file_get_contents($file);
                    if ($html === FALSE)
                      error_pkg($package);
                    else
                      echo $html;
                    $i++;
                    array_push($found, $package);
                    $list .= "<li><a href=\"#$package\">$package</a>\n";
                }
            }
        } else {
            not_found($request);
        }
        pclose($handle);
        return $list;
    } else {
        not_found($request);
    }
}

?>
