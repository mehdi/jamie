/****************************************************************************/
/*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  */
/*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        */
/*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           */
/*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      */
/*                                                software: you can         */
/*   redistribute it and/or modify it under the terms of the GNU General    */
/*   Public License as published by the Free Software Foundation, either    */
/*   version 3 of the License, or (at your option) any later version.       */
/****************************************************************************/

/*
$.ajaxSetup ({
    cache: false
});
*/

function clear_results () {
    $("#results").empty();
    $("#pkglist").empty();
    $("#frame").hide();
    $("#results").append('<div class="nochanges">No changes found!</div>');
}

function set_style(id, attr) {
    document.getElementById(id).style.display = attr;
}

function init () {
    $("#searchq").select().focus();
    $("#clear").click(clear_results);
    $(".hunk_details").hide();
    $(".hunk_header").click(function (obj) {
            var myid = $(this).attr("id") + "_details";
            var style=document.getElementById(myid).style.display;
            if(style == "none") {
                set_style(myid, "block");
            } else {
                set_style(myid, "none");
            }
        });
}

$(document).ready(init);
