
all:
	@echo "Please select a valid action:"
	@egrep '^(##|[a-z%])' Makefile | grep -v 'all:' | sed -e 's/%/native | bytecode/' -e 's/^##/    /' -e 's/^[^#: ].*/ * &/'

exe:
## Compile the program
	ocamlbuild all

maddie:
## Build maddie.native
	ocamlbuild -classic-display $@.native

clean:
## Clean the directory from generated files
	ocamlbuild clean
	find -name "*.html" -or -name "*.png" -or -name "*.dot" -delete
	rm -rf _build

ubuntudiff:
## Generate a multi-mode diff using all.xml
	./maddie.native -f -multi examples/all.xml

publish:
## Publish changes to ubuntudiff.debian.net
	scp web/maddie.php web/maddie.js mehdi@dogguy.org:/home/hosting/debian.net/ubuntudiff/
	scp examples/all.xml mehdi@dogguy.org:/home/hosting/debian.net/ubuntudiff/

headers:
## Update copyright headers
	headache -r -c headache/config -h headache/header *.ml* lib/*.ml* lexers/*.ml* web/maddie/*.js web/maddie/*.php
