(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Printers

type spec = Maddie

exception Bad_format
exception Missing_key of string
exception Empty_configuration

type tree = E of string * tree list | D of string

let config = ref []

let get name =
  try
    List.assoc name !config
  with _ -> ""

let error s = _e s

let in_tree i =
  let el ((_, tag), _) childs = E (tag, childs)  in
  let data d = D d in
  Xmlm.input_doc_tree ~el ~data i

let known_tags =
  [ Maddie , [ "name"; "query"; "description" ];
  ]
let must_tags =
  [ Maddie , [ "name"; "query" ];
  ]

let rec read_config spec kt mt config = function
  | E (name, [ D data ]) :: l when List.mem name kt ->
      if data = "" && List.mem name mt
      then raise (Missing_key name)
      else read_config spec kt mt ((name, data) :: config) l
  | _ -> config

let rec read_maddie config =
  let kt = List.assoc Maddie known_tags in
  let mt = List.assoc Maddie must_tags in
  read_config Maddie kt mt config

let string_of_spec = function
  | Maddie -> "maddie"

let read_spec = function
  | Maddie -> read_maddie

let error_handler spec file f arg =
  try
    f arg
  with
    | Bad_format ->
        _e "E: Bad request format (expected \"%s\") in %s\n%!" spec file;
        exit 1
    | Missing_key key ->
        _e "E: The key \"%s\" is missing in your request file\n%!" key;
        exit 1
    | Empty_configuration ->
        _e "E: Request file contains an empty request\n%!";
        exit 1

let read spec file =
  if not (Sys.file_exists file)
  then Misc.fail file
  else
    try
      let ic = open_in file in
      let i = Xmlm.make_input ~strip:true (`Channel ic) in
      let spec_name = string_of_spec spec in
      let read = read_spec spec in
      let rec lire i =
        if Xmlm.eoi i
        then raise Empty_configuration
        else match snd (in_tree i) with
          | E (name, tl) ->
              if name = spec_name
              then read [] tl
              else lire i
          | D _ -> raise Bad_format in
      config := error_handler spec_name file lire i
    with Xmlm.Error (_, error) ->
      _e "E: XML parser failed: %s\n%!" (Xmlm.error_message error);
      exit 1
