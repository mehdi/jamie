(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Printers

let ($) f x = f x

let uniq l =
  let rec aux acc = function
    | [] -> acc
    | h::l -> if List.mem h l then aux acc l else aux (h::acc) l in
  aux [] l

let hfind = Hashtbl.find
let hmem = Hashtbl.mem
let hadd = Hashtbl.add
let hreplace = Hashtbl.replace
let lmem = List.mem

let default_find hash default pkg =
  try
    hfind hash pkg
  with _ -> default

let escape_name =
  let regexp = Str.regexp "[/\\*&\\?\\$]" in
  Str.global_replace regexp "-"

let elt_print out_channel elt =
  let f = Format.formatter_of_out_channel out_channel in
  Tyxml.Html.pp_elt () f elt

let html_print out_channel elt =
  let f = Format.formatter_of_out_channel out_channel in
  Tyxml.Html.pp () f elt

let dump_html file elt =
  let outchan = open_out file in
  let f = Format.formatter_of_out_channel outchan in
  let () = Tyxml.Html.pp () f elt in
  close_out outchan

let mkdir dir =
  let code = Sys.command (__ "mkdir -p %s" dir) in
  if code <> 0 then begin
    _e "E: Cannot create directory \"%s\"!" dir;
    exit code
  end

let extension s fmt =
  let name = try Filename.chop_extension s with _ -> s in
  try
    let nlen = String.length name in
    let olen = String.length s in
    let ext = String.sub s (nlen + 1) (olen - nlen - 1) in
    name, ext
  with _ ->
    name, fmt

let clean_exit dirty tmp code =
  if dirty
  then _e "W: Temporary file %s left\n%!" tmp
  else Unix.unlink tmp;
  exit code
