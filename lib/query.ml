(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Printers

let sh query =
  let in_c = Unix.open_process_in query in
  let rec read lines in_c =
    try
      read ((input_line in_c) :: lines) in_c
    with End_of_file -> lines
  in
  let packages = read [] in_c in
  close_in in_c;
  packages

let search pkg =
  sh (__ "grep-dctrl -n %s -sPackage %s" pkg !Grab.sources)
