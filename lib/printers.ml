(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

let quiet = ref false
let kuiet = ref false

let __ x = Printf.sprintf x
let _e = Printf.eprintf

let __e x =
  if !quiet || !kuiet
  then Printf.ifprintf stdout x
  else Printf.eprintf x

let error x =
  Printf.ksprintf (fun s -> _e "%s\n%!" s; exit 1) x

let _d x =
  if !quiet || !kuiet
  then Printf.ifprintf stdout x
  else Printf.printf x

let _p x =
  if !kuiet
  then Printf.ifprintf stdout x
  else Printf.printf x
