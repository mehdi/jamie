(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Printers

let cache = ref "_cache"
let mirror = ref "http://http.debian.net/debian"
let areas = ref [ "main"; "contrib"; "non-free" ]
let archs =
  ref [
    "amd64"; "armel"; "armhf"; "i386"; "ia64"; "kfreebsd-amd64";
    "kfreebsd-i386"; "mips"; "mipsel"; "powerpc"; "s390x"; "sparc"
  ]

let sources = ref (Filename.concat !cache "Sources.Debian")
let packages = ref (Filename.concat !cache "Packages.Debian")
let bufsize = 4096
let buffer = Bytes.create bufsize

let reset file =
  try Sys.remove file with _ -> ();
  ignore(Sys.command(__ "touch %s" file))

let write buf data =
  output_string buf data;
  String.length data

let rec copy ic oc =
  let n = Gzip.input ic buffer 0 bufsize in
  if n = 0 then ()
  else begin
    output_bytes oc (Bytes.sub buffer 0 n);
    copy ic oc
  end

let download =
  let errorBuffer = ref "" in
  fun ?(mode = Open_append) ?(compression = true) url file ->
    let tmp, buf =
      if compression then
        Filename.open_temp_file "ocurl_" ""
      else
        file, open_out file
    in
    begin
      try
        let conn = Curl.init () in
        Curl.setopt conn (Curl.CURLOPT_FOLLOWLOCATION true);
        Curl.set_errorbuffer conn errorBuffer;
        Curl.set_writefunction conn (write buf);
        Curl.set_url conn url;
        Curl.perform conn;
        Curl.cleanup conn
      with
        | Curl.CurlException (reason, code, str) ->
            _e "Curl error: %s\n%!" !errorBuffer
        | Failure s ->
            _e "Caught exception: %s\n%!" s
    end;
    flush buf;
    close_out buf;
    errorBuffer := "";
    if compression then begin
      let gzip_ic = Gzip.open_in tmp in
      let out_channel = open_out_gen
        [ Open_wronly; mode; Open_creat; Open_text ]
        0o644
        file
      in
      (try
         copy gzip_ic out_channel
       with
         | End_of_file ->
             ()
         | Gzip.Error error ->
             _e "Gzip data error: %s.\n%!" error
      );
      Gzip.close_in gzip_ic;
      Unix.unlink tmp;
      close_out out_channel
    end

let rec print_l = function
  | [] -> ""
  | [p] -> p
  | p::list -> __ "%s,%s" p (print_l list)

let szget suite file =
  reset file;
  List.iter
    (fun area ->
       let url = __ "%s/dists/%s/%s/source/Sources.gz" !mirror suite area in
       __e "I: Updating Sources(%s; %s) from %s\n%!" suite area url;
       download url file
    )
    !areas

let bzget suite file =
  reset file;
  List.iter
    (fun arch ->
       __e "I: Updating Packages(%s; %s; %s)\n%!" suite arch (print_l !areas);
       List.iter
         (fun area ->
            let url =
              __ "%s/dists/%s/%s/binary-%s/Packages.gz" !mirror suite area arch
            in
            download url file
         )
         !areas
    )
    !archs

let tiny_init () =
  ignore(Sys.command (__ "mkdir -p %s" !cache))

let init activated =
  tiny_init ();
  if activated || not (Sys.file_exists !packages)
     then bzget "unstable" !packages;
  if activated || not (Sys.file_exists !sources )
     then szget "unstable" !sources
