(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

let shapes = Hashtbl.create 42

let _ =
  Hashtbl.add shapes 1 "box";
  Hashtbl.add shapes 2 "polygon";
  Hashtbl.add shapes 3 "ellipse";
  Hashtbl.add shapes 4 "circle";
  Hashtbl.add shapes 5 "component";
  Hashtbl.add shapes 6 "egg";
  Hashtbl.add shapes 7 "triangle";
  Hashtbl.add shapes 8 "plaintext";
  Hashtbl.add shapes 9 "diamond";
  Hashtbl.add shapes 10 "trapezium";
  Hashtbl.add shapes 11 "parallelogram";
  Hashtbl.add shapes 12 "house";
  Hashtbl.add shapes 13 "pentagon";
  Hashtbl.add shapes 14 "hexagon";
  Hashtbl.add shapes 15 "septagon";
  Hashtbl.add shapes 16 "octagon";
  Hashtbl.add shapes 17 "doublecircle";
  Hashtbl.add shapes 18 "doubleoctagon";
  Hashtbl.add shapes 19 "tripleoctagon";
  Hashtbl.add shapes 20 "invtriangle";
  Hashtbl.add shapes 21 "invtrapezium";
  Hashtbl.add shapes 22 "invhouse";
  Hashtbl.add shapes 23 "Mdiamond";
  Hashtbl.add shapes 24 "Msquare";
  Hashtbl.add shapes 25 "Mcircle";
  Hashtbl.add shapes 26 "rect";
  Hashtbl.add shapes 27 "rectangle";
  Hashtbl.add shapes 28 "square";
  Hashtbl.add shapes 29 "none";
  Hashtbl.add shapes 30 "note";
  Hashtbl.add shapes 31 "tab";
  Hashtbl.add shapes 32 "folder";
  Hashtbl.add shapes 33 "box3d";
  Hashtbl.add shapes 34 "filled";
  Hashtbl.add shapes 35 "invisible";
  Hashtbl.add shapes 36 "diagonals";
  Hashtbl.add shapes 37 "rounded";
  Hashtbl.add shapes 38 "dashed";
  Hashtbl.add shapes 49 "dotted";
  Hashtbl.add shapes 40 "solid"

let colors = Hashtbl.create 42

let _ =
  Hashtbl.add colors 1 "beige";
  Hashtbl.add colors 2 "cornflowerblue";
  Hashtbl.add colors 3 "darkolivegreen1";
  Hashtbl.add colors 4 "darkslategray1";
  Hashtbl.add colors 5 "burlywood2";
  Hashtbl.add colors 6 "aquamarine1";
  Hashtbl.add colors 7 "gray64";
  Hashtbl.add colors 8 "lightblue2";
  Hashtbl.add colors 9 "limegreen";
  Hashtbl.add colors 10 "palegreen3";
  Hashtbl.add colors 11 "plum3";
  Hashtbl.add colors 12 "turquoise1";
  Hashtbl.add colors 13 "yellow3";
  Hashtbl.add colors 14 "steelblue2";
  Hashtbl.add colors 15 "springgreen2";
  Hashtbl.add colors 16 "peachpuff2";
  Hashtbl.add colors 17 "sienna3"

let shape id =
  try
    Hashtbl.find shapes id
  with _ -> "box"

let color id =
  try
    Hashtbl.find colors id
  with _ -> "deepskyblue1"
