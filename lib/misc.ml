(****************************************************************************)
(*   _   _ _             _        ___  _  __  __  © 2012-2016 Mehdi Dogguy  *)
(*  | | | | |__ _  _ _ _| |_ _  _|   \(_)/ _|/ _| <mehdi@debian.org>        *)
(*  | |_| | '_ \ || | ' \  _| || | |) | |  _|  _|                           *)
(*   \___/|_.__/\_,_|_||_\__|\_,_|___/|_|_| |_|   This program is free      *)
(*                                                software: you can         *)
(*   redistribute it and/or modify it under the terms of the GNU General    *)
(*   Public License as published by the Free Software Foundation, either    *)
(*   version 3 of the License, or (at your option) any later version.       *)
(****************************************************************************)

open Printf
open Lexing

let (>>) a b = Dose_versioning.Debian.compare a b > 0
let (/) = Filename.concat

let fail file =
  Printers._e "E: File \"%s\" was not found!\n%!" file;
  exit 1

let newline lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum }

let loc_to_string b e =
  let line_start = b.pos_lnum
  and line_end = e.pos_lnum
  and char_start = b.pos_cnum - b.pos_bol
  and char_end = e.pos_cnum - e.pos_bol in
    if (line_start=line_end) then
      Printf.sprintf "line %d, characters %d-%d:"
	line_start
	char_start
	char_end
    else
      Printf.sprintf "line %d, character %d-line %d, character %d:"
	line_start
	char_start
	line_end
	char_end

let with_file file f =
  if Sys.file_exists file then
    let chan = open_in file in
    try
      let res = f chan in
      close_in chan; res
    with e -> close_in chan; raise e
  else fail file

let with_file_out f file =
  try
    let chan = open_out file in
    let _ = f chan in
    close_out chan
  with Sys_error s ->
    Printers._e "E: Something bad happened to %s (%s)!\n" file s

let parse_init entry file =
  with_file file
    begin fun chan ->
      let lexbuf = from_channel chan in
      try
        entry lexbuf
      with Failure _ ->
        let b = (lexeme_start_p lexbuf)
        and e = (lexeme_end_p lexbuf)
        in let loc = loc_to_string b e in
        Printf.eprintf "%s\nE: Error while reading \"%s\" in %s\n%!"
          loc
          (lexeme lexbuf)
          file;
        exit 3
    end
